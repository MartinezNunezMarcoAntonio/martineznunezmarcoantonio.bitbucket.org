var Ajedrez = (function (){

    var _cont_tablero_ajedrez = document.getElementById("tablero");
    var _contenedor_boton    = document.getElementById( "opcion" );
    var _cont_mensaje_error  = document.getElementById( "mensaje" );

    var _tabla_ajedrez;
    const _tamano_fila = 9;
    const _tamano_columna = 8;
    var _simbolo_separador = "|";
    const _representacion_vacio = "∅";
    const _representacion_vacio_html = " ";
    

    var _crear_escenario_ajedrez = function(){
       var _boton_descarga = document.createElement("button");
        _boton_descarga.textContent = "Actualizar";

        _contenedor_boton.appendChild( _boton_descarga );
        _boton_descarga.addEventListener( "click" ,function() {_evento_click(true)} );
        _evento_click(false);
    };



    var _crear_tablero_html = function( matriz_piezas ){
        _tabla_ajedrez = document.createElement("table");
        _cont_tablero_ajedrez.appendChild( _tabla_ajedrez );

        for( var i = matriz_piezas.length-1 ; i >0 ;i-- ){
            var fila_tr = document.createElement( "tr" );
            _tabla_ajedrez.style.fontSize = "35px";
            _tabla_ajedrez.style.fontFamily = "Symbola";
            _tabla_ajedrez.appendChild( fila_tr );

            var mapa_de_piezas = _devolver_fila_pura( matriz_piezas[_tamano_fila - i] ,i );

            mapa_de_piezas.forEach(function(valor, clave) {
                var _elemento = document.createElement( "td" );
                _elemento.textContent = valor;
                _elemento.setAttribute("id" , clave);

                fila_tr.appendChild( _elemento );
            });
        }
    };



    var _actualizar_tabla = function( matriz_piezas ){
        for( var i = matriz_piezas.length-1 ; i >0 ;i-- ){
            var mapa_de_piezas = _devolver_fila_pura( matriz_piezas[_tamano_fila -i] ,i );
            
            mapa_de_piezas.forEach(function(valor, clave) {
                var _pieza = document.getElementById( clave );
                _pieza.textContent = valor;
            });
        }
    };
    /*********MODULO MOVIMIENTO************************ 
    *
    * Crear un objeto con unicamente dos llaves
    * Estas llaves son unicas e irremplazables
    * El contenido esta dentro de un rango
    * 
    *********************************************/
    var _mover_pieza = function( obj ){
        if( _validaciones_generales_objeto( obj ) ){
            var _movimiento_origen = document.getElementById(obj.de);
            var _movimiento_destino = document.getElementById(obj.a);
            
            if( _movimiento_destino === null || _movimiento_origen === null ){
                aviso_error_integridad_valores();
                return;
            }if( _movimiento_destino.textContent !== _representacion_vacio )
            {
                _aviso_error_superposicion();
                return;
            }
            if( _movimiento_origen.textContent === _representacion_vacio ){
                aviso_error_con_mov_vacio();
            }
            else{
                var aux = _movimiento_origen.textContent;
                _movimiento_origen.textContent = _movimiento_destino.textContent;
                _movimiento_destino.textContent = aux;
            }
        }
        else{
            _aviso_error_estructura_objeto();
        }
        
    };

    /*********MODULO CONEXION************************ 
    *
    * Solicitar el archivo CSV
    * CARGAR el archivo y convertirlo a html table
    * 
    *********************************************/
    var _evento_click = function( band ){
        var servidor = "https://martineznunezmarcoantonio.bitbucket.io/";
        var url = servidor + "/csv/tablero.csv" + "?r=" + Math.random();
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200){
                    var _contenido = xhr.responseText;
                    if( _cargar_y_comprobar_Archivo( _contenido ) )
                    {
                        if(band){
                            console.log("Actualizando...");
                            _actualizar_tabla( _matriz_elementos );
                        }
                        else{
                            console.log("Creando...");
                            _crear_tablero_html( _matriz_elementos );
                        }
                    }else{
                        _aviso_error_archivo();
                    }
                }else{
                    _cont_mensaje_error.textContent = "Error :¿Tiene Conexion al servidor?" + this.status + " " + this.statusText + " - " + this.responseURL;
                }
            } 
        };
        xhr.open('GET', url, true);
        xhr.send();
    };

    /*********MODULO CSV************************ 
    *
    * Recibir un String, extraer la cabezera
    * Devolver una matriz de cadenas
    * 
    *********************************************/
    var _matriz_elementos;
    var _cabezera_csv; 

    var _convertir_csv = function( contenido_csv ){
        _matriz_elementos = contenido_csv.split("\n");
        _cabezera_csv = _crear_cabezera( _matriz_elementos[0] );
    };

    var _crear_cabezera = function( cabezera_string ){
        return cabezera_string.split( _simbolo_separador );
    };

    var _cargar_y_comprobar_Archivo = function( contenido_csv ){
        if( contenido_csv!==undefined && contenido_csv!==null ){
            _convertir_csv( contenido_csv );
        }
        else{
            _aviso_error_contenido_archivo();
            return false;
        }
        return _validar_dimension_tablero( _matriz_elementos ) && _validar_caracteres_permitidos( _matriz_elementos ) && _validar_unicidad_caracteres( _matriz_elementos );
    };
    var _devolver_fila_pura = function( fila_csv , num_fila ){
        
        var fila_en_objeto = new Map();
        var _columnas = fila_csv.split( _simbolo_separador );
        for( var i = 0; i < _columnas.length; i++ ){
            var letra = _cabezera_csv[i];
            fila_en_objeto.set( letra+num_fila, _columnas[i]) ;
        }
        return fila_en_objeto;
    };

    /*********MODULO VALIDACION******************* 
    *
    * Este modulo se divide en varias secciones de
    * validacion, validacion del archivo
    * validacion del movimiento
    * 
    *****************PARTE 1****************************/
    var _lista_permitidos = ["a","b","c","d","e","f","g","h","♜","♞","♝","♛","♚","♝","♞","♜","♟","♟","♟","♟","♟","♟","♟","♟","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","∅","♙","♙","♙","♙","♙","♙","♙","♙","♖","♘","♗","♕","♔","♗","♘","♖",_simbolo_separador];
    var _lista_permitidoshtml = ["a","b","c","d","e","f","g","h","&#9820","&#9822","&#9821","&#9819","&#9818","&#9821","&#9822","&#9820","&#9823","&#9823","&#9823","&#9823","&#9823","&#9823","&#9823","&#9823"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","&#9817","&#9817","&#9817","&#9817","&#9817","&#9817","&#9817","&#9817","&#9814","&#9816","&#9815","&#9813","&#9812","&#9815","&#9816","&#9814"]

    var _validar_dimension_tablero = function( _matriz_csv ){
        for( var i = 0; i < _matriz_csv.length ;i++ ){
            if( _matriz_csv[i].length !== ( _tamano_columna *2-1 ) ){
                return false
            }
        }
        return _matriz_csv.length === _tamano_fila;
    };

    var _validar_caracteres_permitidos = function( _matriz_csv ){
        for( var i = 0; i < _matriz_csv.length ;i++ ){
            for( var j = 0; j < _matriz_csv[i].length ;j++ ){
                if( !_lista_permitidos.includes( _matriz_csv[i][j] ) ){
                    return false;
                }
            }
        }
        return true;
    };
    var _validar_unicidad_caracteres = function( _matriz_csv ){
        var linea = _matriz_csv.toString();
        linea = linea.split(_simbolo_separador).join('');
        linea = linea.split(',').join('');

        for( var i =0; i < _lista_permitidos.length-1; i++ ){
            if( linea.indexOf( _lista_permitidos[i]) >= 0 ){
                linea = linea.replace( _lista_permitidos[i] ,"");
            }
            else{
                console.log( "Error los datos no respetan el numero de elementos que deben existir en el tablero -> " + _lista_permitidos[i]);
            }
        }

        return linea.length === 0;
    };
    var _existe_cabezera = function( ) {
        if( _cabezera_csv!==undefined && _cabezera_csv!==null ){
            return true;
        }
        return false;
    };

    /*******************************************************************
    *
    * Validaciones de Movimiento
    * 
    *******************************************************************/
    
    
    var _validar_integriad_valores = function( fuente ,destino ){
        if( fuente.length === 2 && destino.length ===2 ){
            var letra = fuente.substring( 0 ,1 );
            var numero = fuente.substring( 1 ,2 );

            if( !_cabezera_csv.includes( letra ) ){
                aviso_error_generico("error 'de' en la letra(Columna)");
                return false;
            }
            if( numero <1 || numero > 8 ){
                aviso_error_generico("error 'de' en el Numero(Fila)");
                return false;
            }
            var letra = destino.substring( 0 ,1 );
            var numero = destino.substring( 1 ,2 );

            if( !_cabezera_csv.includes( letra ) ){
                aviso_error_generico("error 'a' en la letra(Columna)");
                return false;
            }
            if( numero <1 || numero > 8 ){
                aviso_error_generico("error 'a' en el Numero(Fila)");
                return false;
            }
        }
        else{
            aviso_error_integridad_valores();
            return false;
        }
        return true;
    };
    var _validaciones_generales_objeto = function( obj ){
        if( obj.de === undefined || obj.a === undefined|| obj.de === null || obj.a === null ){
            console.log("Entramos");
            return false;
        }
        return true;
    };



    /*********AVISOS************************ 
    *
    * Mandar avisos al recurso div
    * 
    *********************************************/
    var _aviso_error_solicitud = function( ){
        _cont_mensaje_error.textContent = "Error al procesar la solicitud revisa tu conexion con el servidor";
    };
    var _aviso_error_respuesta = function( ){
        _cont_mensaje_error.textContent = "Error el servidor no ha dado acceso al archivo o no existe";
    };
    var _aviso_error_archivo = function(  ){
        _cont_mensaje_error.textContent = "Error el archivo no es el esperado";
    };
    var _aviso_error_contenido_archivo = function(  ){
        _cont_mensaje_error.textContent = "Error al procesar el contenido del archivo CSV REVISALO!!";
    };
    var _aviso_error_estructura_objeto= function(  ){
        _cont_mensaje_error.textContent = "Error al solicitar un movimiento la estructura de tu peticion no es adecuada";
    };
    var aviso_error_integridad_valores= function( ){
        _cont_mensaje_error.textContent = "Error los datos no coinciden con los elementos maximos de la tabla";
    };
    var _aviso_error_unicidad= function( ){
        _cont_mensaje_error.textContent = "Error los datos no respetan el numero de elementos que deben existir en el tablero";
    };
    var aviso_error_generico = function( _aviso ){
        _cont_mensaje_error.textContent = aviso;
    };
    var aviso_error_con_mov_vacio= function( ){
        _cont_mensaje_error.textContent = "Error No puedes mover algo vacio";
    };
    var _aviso_error_superposicion = function( ){
        _cont_mensaje_error.textContent = "Error No puedes mover sobre un elemento existente";
    };

    /*********EJECUCION Y PUBLICO************** */
    return{
        "mostrarTablero": _crear_escenario_ajedrez,
        "moverPieza" : _mover_pieza
    };

})();

