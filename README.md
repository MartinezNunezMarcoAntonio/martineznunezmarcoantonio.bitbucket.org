**Juego Ajedrez**



*Entrar Al sitio para Probar el Juego [Ajedrez](https://martineznunezmarcoantonio.bitbucket.io/)*

---

## Instrucciones
    **Patron Módulo** -> Ajedrez { mostrarTablero:"" ,moverPieza:"" }
    Para utilizar moverPieza debera respetar la siguiente Sintaxis

    { "de":letra(a-g)Numero(1-8) , "a":letra(a-g)Numero(1-8) }

## Métodos 

Estructura Main.js.

1. **Main** 
2. **Movimiento**
3. **Conexion** 
4. **CSV** 
6. **VALIDACION** 

---
####!!! La separación de estas categorias es para un mejor entendimiento de la estructura del código, Sin embargo No deberian encontrarse en un solo Módulo en si.

##AVISOS
    !!! Los errores respecto a la conexión no se presentan aqui

    var _aviso_error_solicitud = function( ){
        _cont_mensaje_error.textContent = "Error al procesar la solicitud revisa tu conexion con el servidor";
    };
    var _aviso_error_respuesta = function( ){
        _cont_mensaje_error.textContent = "Error el servidor no ha dado acceso al archivo o no existe";
    };
    var _aviso_error_archivo = function(  ){
        _cont_mensaje_error.textContent = "Error el archivo no es el esperado";
    };
    var _aviso_error_contenido_archivo = function(  ){
        _cont_mensaje_error.textContent = "Error al procesar el contenido del archivo CSV REVISALO!!";
    };
    var _aviso_error_estructura_objeto= function(  ){
        _cont_mensaje_error.textContent = "Error al solicitar un movimiento la estructura de tu peticion no es adecuada";
    };
    var aviso_error_integridad_valores= function( ){
        _cont_mensaje_error.textContent = "Error los datos no coinciden con los elementos maximos de la tabla";
    };
    var _aviso_error_unicidad= function( ){
        _cont_mensaje_error.textContent = "Error los datos no respetan el numero de elementos que deben existir en el tablero";
    };
    var aviso_error_generico = function( _aviso ){
        _cont_mensaje_error.textContent = aviso;
    };
    var aviso_error_con_mov_vacio= function( ){
        _cont_mensaje_error.textContent = "Error No puedes mover algo vacio";
    };
    var _aviso_error_superposicion = function( ){
        _cont_mensaje_error.textContent = "Error No puedes mover sobre un elemento existente";
    };